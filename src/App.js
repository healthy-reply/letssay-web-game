import React from 'react';
import './App.css';
import ConfigPanel from './components/config_panel'

function App() {
  return (
    <div className="App">
      <ConfigPanel></ConfigPanel>
    </div>
  );
}

export default App;
