import React, {Component} from 'react';
import Sound from 'react-sound';
import DeviceOrientation, { Orientation } from 'react-screen-orientation'

const imgUri = 'https://i3lab.elet.polimi.it/letssayresources/img/'
const soundUri = 'https://i3lab.elet.polimi.it/letssayresources/sound/'

export default class Reward extends Component {
    constructor(props) {
      super(props);
      this.state = {
        width: 0,
        height:0
      };
      this.handleSongFinishedPlaying = this.handleSongFinishedPlaying.bind(this)
      this.updateWindowDimensions = this.updateWindowDimensions.bind(this)

    }

    componentDidMount() {
      this.updateWindowDimensions();
      window.addEventListener('resize', this.updateWindowDimensions);
    }
  
    componentWillUnmount() {
      window.removeEventListener('resize', this.updateWindowDimensions);
    }

    handleSongFinishedPlaying(){
      this.props.finished()
    }

    updateWindowDimensions() {
      this.setState({ width: window.innerWidth, height: window.innerHeight });
    }
      render(){
          const { reward } = this.props
          const {width, height} = this.state
          return(
            <>
          <div style={{ flexDirection: 'row', width: width, height: height, backgroundoColor:'red'}}>
          <Sound
          url={soundUri + reward.audio + '.wav'}
          playStatus={Sound.status.PLAYING}
          playFromPosition={300 /* in milliseconds */}
          onFinishedPlaying={this.handleSongFinishedPlaying}
        />
        <DeviceOrientation lockOrientation={'landscape'}>
          <Orientation orientation='landscape' alwaysRender={false}>
        <img alt="Reward" styles={styles.background}
          src={imgUri+reward.img+'.png'}>
        </img>
      </Orientation>
      <Orientation orientation='portrait' alwaysRender={false}>
            <div style={{ flexDirection: 'row', width: '100%', height: '100%' }}>
        <img alt="Reward" style={{ height: '100%', width: '100%' }}
          objectfit='cover'
          src={imgUri+reward.img+'.png'}>
        </img>
      </div>
      </Orientation>
      </DeviceOrientation>
      </div>
      </>
          )
      }
    }
    
    const styles ={
      background: {
        width:'100%',
        position: "absolute",
        top: 0,
        left: 0,
        height:'100%',
        objectFit: "cover",
        alignItems: "stretch",
        bottom: 0,
        right: 0,
        zIndex: -1
        }
    }