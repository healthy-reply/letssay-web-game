import React, {Component} from 'react';
import backgroundVideo from './../assets/img/background_video.mp4'
import Sound from 'react-sound';

const imgUri = 'https://i3lab.elet.polimi.it/letssayresources/img/'
const soundUri = 'https://i3lab.elet.polimi.it/letssayresources/sound/'


export default class StoryIntro extends Component {
    constructor(props) {
      super(props);
      this.state = {
        width: 0,
        height:0
      };

      this.handleSongFinishedPlaying = this.handleSongFinishedPlaying.bind(this)
      this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
    }

    componentDidMount() {
      this.updateWindowDimensions();
      window.addEventListener('resize', this.updateWindowDimensions);
    }
  
    componentWillUnmount() {
      window.removeEventListener('resize', this.updateWindowDimensions);
    }
  
    updateWindowDimensions() {
      this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    handleSongFinishedPlaying(){
      this.props.finished()
    }
      render(){
          const { story_intro } = this.props
          const { width, height } = this.state

          return(
            <div style={{ flexDirection: 'row', width: width, height: height }}>
             <Sound
                url= {soundUri+this.props.story_intro.audio+'.wav'}
                playStatus={Sound.status.PLAYING}
                playFromPosition={300 /* in milliseconds */}
                onLoading={this.handleSongLoading}
                onPlaying={this.handleSongPlaying}
                onFinishedPlaying={this.handleSongFinishedPlaying}
              />
            <video autoPlay loop muted style={styles.backgroundVideo}>
                    <source src={backgroundVideo} type='video/mp4'/>
            </video>
            <div style={{ flexDirection: 'row', width: width, height: height }}>
        {story_intro.img !== "" ?
        <img alt ={"Story Intro"} style={{ height: '100%', width: '100%' }}
          objectfit='contain'
          src={imgUri+story_intro.img+'.png'}>
        </img> : 
        null
      }
        
      </div>
      </div>
          )
      }
    }
    
    const styles = {
      container: {
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        dislay: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      },
      backgroundVideo: {
        width:'100%',
        position: "absolute",
        top: 0,
        left: 0,
        height:'100%',
        objectFit: "cover",
        alignItems: "stretch",
        bottom: 0,
        right: 0,
        zIndex: -1
        }
    }