import React, {Component} from 'react';
import Sound from 'react-sound';
import backgroundVideo from './../assets/img/background_video.mp4'
import pimpky from './../assets/img/pimpky.mp4'

const imgUri = 'https://i3lab.elet.polimi.it/letssayresources/img/'
const soundUri = 'https://i3lab.elet.polimi.it/letssayresources/sound/'


export default class Intro extends Component {
    constructor(props) {
      super(props);
      this.state = {
        width: 0,
        height: 0
      }
      this.handleSongFinishedPlaying = this.handleSongFinishedPlaying.bind(this)
      this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
    }
            
    componentDidMount() {
      this.updateWindowDimensions();
      window.addEventListener('resize', this.updateWindowDimensions);
    }
  
    componentWillUnmount() {
      window.removeEventListener('resize', this.updateWindowDimensions);
    }
  
    updateWindowDimensions() {
      this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    handleSongFinishedPlaying(){
      this.props.finished()
    }

      render(){
        const { intro } = this.props
        const { width, height } = this.state
          return(
            <div style={{ flexDirection: 'row', width: width, height: height }}>
              <video autoPlay loop muted style={styles.backgroundVideo}>
                    <source src={backgroundVideo} type='video/mp4'/>
                </video>
              <Sound
                url= {soundUri+this.props.intro.audio+'.wav'}
                playStatus={Sound.status.PLAYING}
                playFromPosition={300 /* in milliseconds */}
                onLoading={this.handleSongLoading}
                onPlaying={this.handleSongPlaying}
                onFinishedPlaying={this.handleSongFinishedPlaying}
              />
        {intro.img !== "" ?
        intro.img === "pimpky" ?
        <video autoPlay loop muted style={styles.backgroundVideo}>
                    <source src={pimpky} type='video/mp4'/>
                </video>     
        :
        <img alt={"Intro"} style={{ height: '100%', width: '100%' }}
          objectfit='cover'
          src={imgUri+intro.img+'.png'}>
        </img> : 
        null}
        
      </div>
          )
      }
    }
    
    const styles = {
      container: {
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        dislay: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      },
      backgroundVideo: {
        width:'100%',
        position: "absolute",
        top: 0,
        left: 0,
        height:'100%',
        objectFit: "cover",
        alignItems: "stretch",
        bottom: 0,
        right: 0,
        zIndex: -1
        }
    }