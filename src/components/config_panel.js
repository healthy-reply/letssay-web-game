import React, { Component } from 'react';
import { PINK, ORANGE } from './../assets/colors'
import ConfigurationFile from './../assets/config_file.json'
import Activity from './activity';
import backgroundVideo from './../assets/img/background_video.mp4'
import start from './../assets/img/start.png'


const socketEndpoint = 'wss://letssay.ticuro.io/ihp-letssay/letssaygame/';
const controlPanel = 'https://letssay.ticuro.io/letssay/pages/games';

export default class ConfigPanel extends Component {
    constructor(props) {
        super(props);
        this.onStructureChange = this.onStructureChange.bind(this)
        this.onActivityTypeChange = this.onActivityTypeChange.bind(this)
        this.onEpisodeChange = this.onEpisodeChange.bind(this)
        this.onTestChange = this.onTestChange.bind(this)
        this.onShowTextChange = this.onShowTextChange.bind(this)
        this.resetCommand = this.resetCommand.bind(this)
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this)

        this.state = {
            configurationFile: ConfigurationFile,
            activity_type: null,
            structure: null,
            test: null,
            episode: null,
            index: 0,
            paused: false,
            flag: false,
            showText: null,
            connOpen: false,
            command: null,
            patientId: null,
            sessionId: null,
            welcome: "",
            width: 0,
            height: 0,
            challenge: {}
        };
    }

    sendStarted() {
        ("send STARTED")
        this.socket.send(JSON.stringify({ patientId: this.state.patientId, sessionId: this.state.sessionId, action: "STARTED" }));
    }

    sendData = (data) => {
        console.log("send FINISHED and data ", data)
        this.socket.send(JSON.stringify({ patientId: this.state.patientId, sessionId: this.state.sessionId, action: "FINISHED", data: data }));
    }

    sendCompleted() {
        console.log("send COMPLETED")
        this.socket.send(JSON.stringify({ patientId: this.state.patientId, sessionId: this.state.sessionId, action: "COMPLETED" }));
    }

    sendTerminated() {
        console.log("send TERMINATED")
        this.socket.send(JSON.stringify({ patientId: this.state.patientId, sessionId: this.state.sessionId, action: "TERMINATED" }));
    }

    sendClose() {
        console.log("send CLOSED")
        this.setState({ paused: false })
        this.socket.send(JSON.stringify({ patientId: this.state.patientId, sessionId: this.state.sessionId, action: "CLOSED" }));
    }

    componentDidMount() {
        this.getWelcome()
        this.getPatientId()
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }


    getWelcome() {
        const array = ["Ciao!", "Bentornato!", "Hey!", "Che bello rivederti!"]
        const rand = Math.floor(Math.random() * 4);
        this.setState({ welcome: array[rand] });
    }

    getPatientId = () => {
        let url = new URL(window.location.href);
        this.setState({ patientId: url.searchParams.get('patientId') })

        if (this.state.connOpen === false) {
            this.socket = new WebSocket(socketEndpoint)

        } else {
            console.log("send OPEN, patientId ", this.state.patientId)
            this.socket.send(JSON.stringify({ patientId: this.state.patientId, action: "OPEN" })); // send a message
        }

        this.socket.onopen = () => {
            // connection opened
            console.log("send OPEN, patientId ", this.state.patientId)
            this.socket.send(JSON.stringify({ patientId: this.state.patientId, action: "OPEN" })); // send a message
            this.setState({ connOpen: true })
        };
        this.socket.onmessage = (e) => {
            // a message was received
            console.log("Message received", JSON.parse(e.data))
            switch (JSON.parse(e.data).action) {
                case "PLAY":
                    this.resetCommand()
                    const at = this.state.configurationFile.find(act => Object.keys(act).toString() === JSON.parse(e.data).config.activityType)
                    const s = at[JSON.parse(e.data).config.activityType].find(act => Object.keys(act).toString() === JSON.parse(e.data).config.structure)
                    const t = s[JSON.parse(e.data).config.structure].find(act => Object.keys(act).toString() === JSON.parse(e.data).config.test)[JSON.parse(e.data).config.test]
                    if (JSON.parse(e.data).config.episode != null) {
                        const ep = t.content.find(act => Object.keys(act).toString() === JSON.parse(e.data).config.episode)[JSON.parse(e.data).config.episode]
                        this.setState({ episode: ep })
                    }else{
                        this.setState({ episode: null })
                    }
                    this.setState({
                        patientId: JSON.parse(e.data).patientId,
                        sessionId: JSON.parse(e.data).sessionId,
                        activity_type: at,
                        structure: s,
                        test: t,
                        showText: JSON.parse(e.data).config.showText,
                        paused: JSON.parse(e.data).config.status === "PAUSED",
                        index: JSON.parse(e.data).config.miniTaskId === null || JSON.parse(e.data).config.activityType === "Assessment" ? 0 : JSON.parse(e.data).config.miniTaskId,
                    }, () => this.launchActivity())
                    break;
                case "REPEAT":
                    this.setState({ command: "REPEAT" })
                    break;
                case "NEXT":
                    this.setState({ command: "NEXT" })
                    break;
                case "PREVIOUS":
                    this.setState({ command: "PREVIOUS" })
                    break;
                case "PAUSE":
                    this.setState({ command: "PAUSE", flag: false })
                    break;
                case "TERMINATE":
                    this.setState({ command: "TERMINATE" })
                    break;
                default:
                    break;

            }
        };

        this.socket.onerror = (e) => {
            // an error occurred
            console.log("onError", e.message);
        };

        this.socket.onclose = (e) => {
            // connection closed
            console.log("send CLOSED")
            this.socket.send(JSON.stringify({ patientId: this.state.patientId, action: "CLOSED" })); // send a message
            this.setState({ connOpen: false, patientId: null })
        };
    };


    translate(string) {
        switch (string) {
            case "training":
                return "Esercitazione";
            case "assessment":
                return "Verifica";
            default:
                return ""

        }
    }

    idToType(id) {
        switch (id) {
            case 0:
                return "training";
            case 1:
                return "assessment";
            default:
                return ""
        }
    }

    launchActivity() {
        this.setState({ flag: true })
        this.sendStarted()
    }

    resetCommand() {
        this.setState({ command: null })
    }

    onActivityTypeChange(value) {
        this.setState({
            activity_type: value
        });
    }

    onShowTextChange(value) {
        this.setState({
            showText: value
        });
    }
    onStructureChange(value) {
        this.setState({
            structure: value
        });

    }

    onTestChange(value) {
        this.setState({
            test: value
        });
    }

    onEpisodeChange(value) {
        this.setState({
            episode: value
        });
    }

    render() {
        const { test, episode, showText, flag, index, width, height, paused, challenge } = this.state;
        const { navigation } = this.props;
        if (flag) {
            return (
                <>
                    <Activity
                        type={test["activity_type"]}
                        navigation={navigation}
                        activity={episode === null ? test : episode}
                        showText={showText}
                        index={index}
                        command={this.state.command}
                        paused={paused}
                        challenge={challenge}
                        resetCommand={() => this.resetCommand()}
                        sendStarted={() => this.sendStarted()}
                        sendData={(data) => this.sendData(data)}
                        sendCompleted={() => this.sendCompleted()}
                        sendTerminated={() => this.sendTerminated()}
                        stopGame={() => {
                            this.setState({ flag: false })
                            this.sendClose()
                            window.open(controlPanel, "_blank")
                        }}>
                    </Activity></>)
        } else {
            return (
                <div style={{ flexDirection: 'row', width: width, height: height }}>
                    <video autoPlay loop muted style={styles.backgroundVideo}>
                        <source src={backgroundVideo} type='video/mp4' />
                    </video>
                    <div style={{ display: 'flex', flexDirection: 'column', height: '100vh', alignItems: 'center', justifyContent: 'center' }}>
                        <p style={{ color: 'white', fontSize: 40, fontWeight: '700' }}>{this.state.welcome}</p>
                        <p style={{ color: 'white', fontSize: 20 }}>Apri il tuo pannello di controllo per iniziare a giocare con Letssay</p>
                        <button onClick={() => window.open(controlPanel, "_blank")}
                        style={{border: 'none', backgroundColor:'transparent', outline:'none'}}>
                            <img alt={"Start Button"} src={start} style={{ width: 80, height: 80, marginTop: 30}} />
                        </button>
                    </div>
                </div>
            )
        }
    }
}

const styles = {
    button: {
        padding: 50,
        margin: 10,
        backgroundColor: ORANGE,
    },
    buttonText: {
        color: 'white',
        fontSize: 25,
    },
    row: {
        display: 'flex',
        flexDirection: 'row',
        maxWidth: 500,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 30
    },
    column: {
        display: 'flex',
        flexDirection: 'column',
        marginHorizontal: 10
    },
    title: {
        color: PINK,
        fontWeight: '500',
        fontSize: 40,
        marginBottom: 20
    },
    container: {
        flexDirection: 'column',
        width: '100%',
        height: '100%',
        dislay: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    backgroundVideo: {
        width: '100%',
        position: "absolute",
        top: 0,
        left: 0,
        height: '100%',
        objectFit: "cover",
        alignItems: "stretch",
        bottom: 0,
        right: 0,
        zIndex: -1
    }
}