import React, { Component } from 'react';
import play from './../assets/img/play.png'
import Sound from 'react-sound';
import DeviceOrientation, { Orientation } from 'react-screen-orientation'

const imgUri = 'https://i3lab.elet.polimi.it/letssayresources/img/'
const soundUri = 'https://i3lab.elet.polimi.it/letssayresources/sound/'


export default class Task extends Component {
  constructor(props) {
    super(props);
    this.giveAnswer = this.giveAnswer.bind(this)
    this.state = {
      loopingSound: undefined,
      tests: {},
      start: null,
      stop: false,
      play: false,
      repeat: this.props.repeat,
      playSound: true,
      width: 0,
      height: 0,
      tryAgain: false
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);

  }

  componentDidUpdate(prevProps) {
    if (prevProps.command !== this.props.command) {
      this.setState({ play: false, tryAgain: false, playSound: true, start: new Date() })
    }
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
    this.setState({ start: new Date() })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  giveAnswer(option) {
    this.setState({ stop: true })
    var time = 0
    if (this.state.start != null) {
      var finish = new Date()
      time = finish.getTime() - this.state.start.getTime();
      this.setState({ start: null })
    }
    var data = {
      itemSelected: option,
      correct: option.toLowerCase().includes("target"),
      time: time
    }
    this.props.sendData(data)
    if (option.toLowerCase().includes("target")) {
      this.props.increaseIndex()
      this.setState({ play: false, tryAgain: false, playSound: true, start: new Date() })
    } else {
      this.setState({ stop: false, tryAgain: true, playSound: false, start: this.state.start })
    }
  }


  render() {
    const { task, background, showText } = this.props
    const { playSound, width, height, tryAgain } = this.state
    return (
      <>
        {playSound && <Sound
          url={soundUri + task.audio + '.wav'}
          playStatus={Sound.status.PLAYING}
          playFromPosition={800 /* in milliseconds */}
          onFinishedPlaying={() => {
            this.setState({ playSound: false })
            if (this.props.options[0] === "") {
              this.props.increaseIndex()
              this.setState({ play: false, tryAgain: false, playSound: true, start: new Date() })
            }
          }}
          onLoad={() =>
            this.setState({ play: true })}
        />}
        {tryAgain && <Sound
          url={soundUri + 'Prova_Ancora.wav'}
          playStatus={Sound.status.PLAYING}
          playFromPosition={300 /* in milliseconds */}
          onFinishedPlaying={() => {
            this.setState({ tryAgain: false })
          }}
        />}
        <DeviceOrientation lockOrientation={'landscape'}>
          <Orientation orientation='landscape' alwaysRender={false}>
            <div style={{ display: 'flex', flexDirection: 'row', width: width, height: height, background: background }}>
              <img alt="Main" style={{ width: this.props.options[0] === "" ? width : width * 0.8, border: 0, objectFit: 'contain' }} src={imgUri + task.main_img + '.png'} />
              {this.props.options[0] === "" ? null :
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                  <button onClick={() => { this.setState({ stop: false, playSound: true }) }}
                    style={{
                      position: 'absolute',
                      left: 30,
                      bottom: 20,
                      zIndex: 40,
                      border: 'none',
                      backgroundColor: 'transparent',
                      outline: 'none'
                    }}>
                    <img alt="Play Button" src={play} style={{ width: height / 8, height: height / 8 }} />
                  </button>
                </div>}
              {showText && this.state.play ?
                <p style={{ position: 'absolute', left: 0, right: this.props.options[0] === "" ? 0 : '20%', top: 30, margin: 'auto', width: this.props.options[0] === "" ? '80%' : '65%', fontSize: 20, color: '#FF9A17', textAlign: 'center', fontWeight: '600', backgroundColor: 'rgba(277,277,277,0.6)', padding: 20, borderRadius: 50 }}>{task.text.toUpperCase()}</p>
                : null}
              {this.props.options[0] === "" ? null :
                <div
                  style={{ display: 'flex', width: width * 0.2, height: '100%', flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center', paddingVertical: 5 }}>
                  {this.props.options.map((option, key) => {
                    return <button key={key} onClick={() => this.giveAnswer(option, task)}
                      style={{
                        height: '30%',
                        width: '90%',
                        objectFit: 'contain',
                        border: 'none',
                        backgroundColor: 'transparent',
                        outline: 'none'
                      }}>
                      <img alt="Option" src={imgUri + option + '.png'}
                        style={{
                          width: "100%",
                          height: "100%"
                        }}></img>
                    </button>
                  })}
                </div>}
            </div>
          </Orientation>
          <Orientation orientation='portrait' alwaysRender={false}>
            <div style={{ display: 'flex', flexDirection: 'column', width: width, height: height, background: background, alignItems: 'center', justifyContent: 'center' }}>
              <img alt="Main" style={{ width: width, border: 0, objectFit: 'contain' }}
                src={imgUri + task.main_img + '.png'}>
              </img>
              {this.props.options[0] === "" ? null :
                <button onClick={() => {
                  this.setState({ stop: false, playSound: true })
                }}
                  style={{
                    position: 'absolute',
                    left: 30,
                    bottom: 20,
                    zIndex: 40,
                    border: 'none',
                    backgroundColor: 'transparent',
                    outline: 'none'
                  }}>
                  <div>
                    <img alt="Play Button" src={play} style={{ width: width / 6, height: width / 6 }}></img>
                  </div>
                </button>}
              {showText && this.state.play ?
                <p style={{ position: 'absolute', left: 0, right: this.props.options[0] === "" ? 0 : '20%', top: 30, margin: 'auto', width: '80%', fontSize: 20, color: '#FF9A17', textAlign: 'center', fontWeight: '600', backgroundColor: 'rgba(277,277,277,0.6)', padding: 20, borderRadius: 50 }}>{task.text.toUpperCase()}</p>
                : null}
              {this.props.options[0] === "" ? null :
                <div
                  style={{ display: 'flex', width: width, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', paddingVertical: 5 }}>
                  {this.props.options.map((option, key) => {
                    return <button key={key} onClick={() => this.giveAnswer(option, task)}
                      style={{
                        height: '90%',
                        width: '90%',
                        border: 'none',
                        backgroundColor: 'transparent',
                        outline: 'none'
                      }}>
                      <img alt="Option" src={imgUri + option + '.png'} style={{ width: "100%", height: "100%", objectFit: 'contain' }}></img>
                    </button>
                  })}
                </div>}
            </div>
          </Orientation>
        </DeviceOrientation>
      </>
    )
  }
}
