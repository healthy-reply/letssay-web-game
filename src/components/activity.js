import React, { Component } from 'react';

import Task from '../components/task'
import TaskAlternate from '../components/taskAlternate'
import TaskComposed from '../components/taskComposed'
import TaskFull from '../components/taskFull'
import Intro from '../components/intro';
import StoryIntro from '../components/story_intro';
import Reward from '../components/reward';


export default class Activity extends Component {
  constructor(props) {
    super(props);
    this.increaseIndex = this.increaseIndex.bind(this)
    this.shuffle = this.shuffle.bind(this)
    this.finishedReward = this.finishedReward.bind(this)
    this.finishedTasks = this.finishedTasks.bind(this)
    this.finishedIntro = this.finishedIntro.bind(this)
    this.finishedStoryIntro = this.finishedStoryIntro.bind(this)
    this.sendData = this.sendData.bind(this)
    this.sendDataAfterCommand = this.sendDataAfterCommand.bind(this)
    this.launchCommand = this.launchCommand.bind(this)
    this.repeat = this.repeat.bind(this)
    this.next = this.next.bind(this)
    this.previous = this.previous.bind(this)
    this.terminate = this.terminate.bind(this)

    this.state = {
      index: this.props.index,
      storyIntro: this.props.paused ? false : true, 
      intro: this.props.paused ? false : true,  
      task: this.props.paused ? true : false, 
      reward: false,
      command: this.props.command,
      repeat: false
    };
  }

  shuffle(arr) {
    var i,
      j,
      temp;
    for (i = arr.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      temp = arr[i];
      arr[i] = arr[j];
      arr[j] = temp;
    }
    return arr;
  };


  componentDidUpdate(prevProps) {
    if (prevProps.command !== this.props.command) {
      this.launchCommand(this.props.command);
    }
  }

  launchCommand(command) {
    switch (command) {
      case null:
        break;
      case "REPEAT":
        this.repeat()
        break;
      case "NEXT":
        this.next()
        break;
      case "PREVIOUS":
        this.previous()
        break;
      case "TERMINATE":
        this.terminate()
        break;
        default:
          break;
    }
  }

  increaseIndex() {
    const { activity } = this.props
    const { index } = this.state
    if (index !== activity.tasks.length - 1) {
      this.setState({ index: this.state.index + 1 })
    } else {
      this.finishedTasks()
    }
  }

  next() {
    const { activity } = this.props
    const { index } = this.state
    this.sendDataAfterCommand()
    if (index !== activity.tasks.length - 1) {
      this.setState({ index: this.state.index + 1 }, () => this.props.resetCommand())
    } else {
      this.setState({ task: false, reward: false })
      this.finishedTasks()
      this.props.stopGame()
    }
  }

  previous() {
    const { index } = this.state
    this.sendDataAfterCommand()
    if (index > 0) {
      this.setState({ index: this.state.index - 1 }, () => this.props.resetCommand())
    } else {
      this.setState({ index: this.state.index })
    }
  }

  repeat() {
    this.sendDataAfterCommand()
    this.setState({ index: 0 }, () => this.props.resetCommand())
  }

  terminate() {
    this.sendDataAfterCommand()
    this.setState({ intro: false, task: false, reward: false }, () => this.props.resetCommand())
    this.props.sendTerminated()
    this.props.stopGame()
  }

  finishedStoryIntro() {
    this.setState({ storyIntro: false, intro: true })
  }

  finishedIntro() {
    this.setState({ intro: false, task: true })
  }

  finishedTasks() {
    this.setState({ task: false, reward: true })
    this.props.sendCompleted()
  }

  finishedReward() {
    this.setState({ reward: false })
    this.props.stopGame()
  }

  sendData(data) {
    const finalData = { itemSelected: data.itemSelected, correct: data.correct, time: data.time, miniTask: this.state.index }
    this.props.sendData(finalData)
  }

  sendDataAfterCommand() {
    const finalData = { itemSelected: null, correct: null, time: null, miniTask: this.state.index }
    this.props.sendData(finalData)
  }

  render() {
    const { type, activity, showText } = this.props
    const { index, repeat } = this.state
    switch (type) {
      case "multiple_choice":
        if (this.state.intro === true) {
          if (this.state.storyIntro === true) {
            if (activity.story_intro && activity.story_intro.audio !== "") {
              return <StoryIntro story_intro={activity.story_intro} finished={this.finishedStoryIntro} />
            }else{
              this.finishedStoryIntro()
              return null
            }
          }
          else {
            if (activity.intro.audio !== "") {
              return <Intro intro={activity.intro} finished={this.finishedIntro} />
            } else {
              this.finishedIntro()
              return null
            }
          }
        }
        else if (this.state.reward === true) {
          return <Reward story_closing={activity.story_closing} reward={activity.reward} finished={this.finishedReward} />
        } else if (this.state.task === true) {
          return <Task task={activity.tasks[index]} background={activity.background} colors={activity.colors} increaseIndex={this.increaseIndex} options={this.shuffle([activity.tasks[index].distr_img, activity.tasks[index].comp_img, activity.tasks[index].target_img])} sendData={(data) => this.sendData(data)} showText={showText} repeat={repeat} command={this.props.command} />
        } else {
          return null
        }
      case "multiple_choice_full":
        if (this.state.intro === true) {
          if (activity.intro.audio !== "") {
            return <Intro intro={activity.intro} finished={this.finishedIntro} />
          } else {
            this.finishedIntro()
            return null
          }
        } else if (this.state.task === true) {
          return <TaskFull task={activity.tasks[index]} background={activity.background} colors={activity.colors} increaseIndex={this.increaseIndex} options={this.shuffle([activity.tasks[index].distr_img, activity.tasks[index].comp_img, activity.tasks[index].target_img])} sendData={this.sendData} showText={showText} command={this.props.command} />
        } else {
          this.props.stopGame()
          return null
        }
      case "multiple_choice_alt":
        if (this.state.intro === true) {
          if (activity.intro.audio !== "") {
            return <Intro intro={activity.intro} finished={this.finishedIntro} />
          } else {
            this.finishedIntro()
            return null
          }
        } else if (this.state.task === true) {
          return <TaskAlternate task={activity.tasks[index]} background={activity.background} colors={activity.colors} increaseIndex={this.increaseIndex} options={this.shuffle([activity.tasks[index].distr_img, activity.tasks[index].comp_img, activity.tasks[index].target_img])} sendData={this.sendData} showText={showText} command={this.props.command} />
        } else {
          this.props.stopGame()
          return null
        }
      case "composed":
        if (this.state.intro === true) {
          if (activity.intro.audio !== "") {
            return <Intro intro={activity.intro} finished={this.finishedIntro} />
          } else {
            this.finishedIntro()
            return null
          }
        } else if (this.state.reward === true) {
          this.finishedReward()
          return null
        } else if (this.state.task === true) {
          return <TaskComposed task={activity.tasks[index]} background={activity.background} colors={activity.colors} increaseIndex={this.increaseIndex} options={[activity.tasks[index].first_img, activity.tasks[index].second_img, activity.tasks[index].third_img]} sendData={this.sendData} showText={showText} command={this.props.command} />
        } else {
          this.props.stopGame()
          return null
        }
        default:
        this.props.stopGame()
        return null
    }
  }

}
