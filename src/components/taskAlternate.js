import React, { Component } from 'react';
import play from './../assets/img/play.png'
import Sound from 'react-sound';
import DeviceOrientation, { Orientation } from 'react-screen-orientation'
import backgroundVideo from './../assets/img/background_video.mp4'


const imgUri = 'https://i3lab.elet.polimi.it/letssayresources/img/'
const soundUri = 'https://i3lab.elet.polimi.it/letssayresources/sound/'


export default class TaskAlternate extends Component {
  constructor(props) {
    super(props);
    this.giveAnswer = this.giveAnswer.bind(this)
    this.state = {
      loopingSound: undefined,
      tests: {},
      start: null,
      stop: null,
      play: false,
      playSound: true,
      width: 0,
      height: 0,
      tryAgain: false
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);

  }

  componentDidUpdate(prevProps) {
    if (prevProps.command !== this.props.command) {
      this.setState({ play: false, tryAgain: false, playSound: true, start: new Date() })
    }
  }

  giveAnswer(option, task) {
    this.setState({ stop: true })
    var time = 0
    if (this.state.start !== null) {
      var finish = new Date()
      time = finish.getTime() - this.state.start.getTime();
      this.setState({ start: null })
    }
    var data = {
      itemSelected: option,
      correct: option.toLowerCase().includes("target"),
      time: time
    }
    this.props.sendData(data)
    this.props.increaseIndex()
    this.setState({ play: false, tryAgain: false, playSound: true, start: new Date() })
  }


  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
    this.setState({ start: new Date() })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }


  render() {

    const { task, showText } = this.props
    const { playSound, width, height } = this.state
    return (
      <>
        {playSound && <Sound
          url={soundUri + task.audio + '.wav'}
          playStatus={Sound.status.PLAYING}
          playFromPosition={800 /* in milliseconds */}
          onFinishedPlaying={() => {
            this.setState({ playSound: false })
            if (task.main_img !== "") {
              this.props.increaseIndex()
              this.setState({ play: false, playSound: true, start: new Date() })
            }
          }}
          onLoad={() =>
            this.setState({ play: true })}
        />}
        <DeviceOrientation lockOrientation={'landscape'}>
          <Orientation orientation='landscape' alwaysRender={false}>
            <div
              style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', width: width, height: height }}>
              <video autoPlay loop muted style={styles.backgroundVideo}>
                <source src={backgroundVideo} type='video/mp4' />
              </video>
              {task.main_img !== "" ?
                <img alt="Main" style={{ height: '90%', objectFit: 'contain' }}
                  src={imgUri + task.main_img + '.png'}>
                </img> :
                this.props.options.map((option, key) => {
                  if (option !== undefined) {
                    return <button key={key}
                      style={{
                        border: 'none',
                        backgroundColor: 'transparent',
                        outline: 'none'
                      }}
                      onClick={() => this.giveAnswer(option, task)}>
                      <img alt="Option" src={imgUri + option + '.png'} style={{ width: '80%', objectFit: 'contain' }}></img>
                    </button>
                  } else {
                    return null
                  }
                }
                )
              }
              {task.main_img !== "" ?
                null :
                <button style={{
                  position: 'absolute',
                  left: 80,
                  bottom: 30,
                  border: 'none',
                  backgroundColor: 'transparent',
                  outline: 'none'
                }}
                  onClick={() => this.setState({ stop: false, playSound: true })}>
                  <img alt="Play Button" src={play} style={{ width: 70, height: 70, marginRight: 20 }}></img>
                </button>}
              {showText && this.state.play && task.text !== "" ?
                <p style={{ position: 'absolute', left: 0, right: 0, top: 30, margin: 'auto', width: this.props.options[0] === "" ? '80%' : '75%', fontSize: 20, color: '#FF9A17', textAlign: 'center', fontWeight: '600', backgroundColor: 'rgba(277,277,277,0.6)', padding: 20, borderRadius: 50 }}>{task.text.toUpperCase()}</p>
                : null}
            </div>
          </Orientation>
          <Orientation orientation='portrait' alwaysRender={false}>
            <div
              style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', width: width, height: height }}>
              <video autoPlay loop muted style={styles.backgroundVideo}>
                <source src={backgroundVideo} type='video/mp4' />
              </video>
              {task.main_img !== "" ?
                <img alt="Main" style={{ width: '100%', objectFit: 'contain' }}
                  src={imgUri + task.main_img + '.png'}>
                </img> :
                this.props.options.map((option, key) => {
                  if (option !== undefined) {
                    return <button key={key} onClick={() => this.giveAnswer(option, task)}
                      style={{
                        border: 'none',
                        backgroundColor: 'transparent',
                        outline: 'none'
                      }}>
                      <img alt="Option" src={imgUri + option + '.png'} style={{ width: '80%', objectFit: 'contain' }}></img>
                    </button>
                  } else {
                    return null
                  }
                }
                )
              }
              {task.main_img !== "" ?
                null :
                <button style={{
                  position: 'absolute',
                  left: 40,
                  bottom: 30,
                  border: 'none',
                  backgroundColor: 'transparent',
                  outline: 'none'
                }}
                  onClick={() => this.setState({ stop: false, playSound: true })}>
                  <img alt="Play Button" src={play} style={{ width: 70, height: 70, marginRight: 20 }}></img>
                </button>}
              {showText && this.state.play && task.text !== "" ?
                <p style={{ position: 'absolute', left: 0, right: 0, top: 30, margin: 'auto', width: this.props.options[0] === "" ? '80%' : '75%', fontSize: 20, color: '#FF9A17', textAlign: 'center', fontWeight: '600', backgroundColor: 'rgba(277,277,277,0.6)', padding: 20, borderRadius: 50 }}>{task.text.toUpperCase()}</p>
                : null}
            </div>
          </Orientation>
        </DeviceOrientation>
      </>
    )
  }
}


const styles = {
  container: {
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    dislay: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundVideo: {
    width: '100%',
    position: "absolute",
    top: 0,
    left: 0,
    height: '100%',
    objectFit: "cover",
    alignItems: "stretch",
    bottom: 0,
    right: 0,
    zIndex: -1
  }
}